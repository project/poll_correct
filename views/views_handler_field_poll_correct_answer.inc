<?php
/**
 * @file
 * Definition of views_handler_field_poll_correct.
 */

/**
 * A handler to return the answer based on it's ID.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_poll_correct_answer extends views_handler_field {
  /**
   * Constructor to provide additional field to add.
   *
   * This constructer assumes the poll table. When using the poll_choice table,
   * we'll need to be more specific.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array(
      'table' => 'node',
      'field' => 'nid',
    );
  }

  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  function render($values) {
    $chid = $this->get_value($values);
    $nid = $values->nid;

    // @todo There must be a smarter way to get this join into the views query!
    if (is_numeric($chid) && is_numeric($nid)) {
      // Select the value from the poll_choices table.
      $text = db_query("SELECT chtext FROM {poll_choice} WHERE chid = :chid AND nid = :nid",
        array(':chid' => $chid, ':nid' => $nid))->fetchField();
      return $this->sanitize_value($text);
    }

    return $this->sanitize_value($chid);
  }
}


<?php
/**
 * @file
 * Provide views integration for poll correct answers.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_handlers().
 */
function poll_correct_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'poll_correct') . '/views',
    ),
    'handlers' => array(
      'views_handler_field_poll_correct_answer' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function poll_correct_views_data() {
  // Add a field for the poll correct answer.
  $data['poll']['correct'] = array(
    'title' => t('Correct answer'),
    'help' => t('The correct answer for the poll.'),
    'field' => array(
      'handler' => 'views_handler_field_poll_correct_answer',
      'click sortable' => TRUE,
    ),
    /*
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    */
  );

  return $data;
}

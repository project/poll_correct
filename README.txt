Poll correct answer
===================

This module allows you to specify the correct answer for a poll question. When
the correct answer has been selected, poll takers will see the correct answer
displayed above the poll results.

This module is an add-on to the Poll module provided with Drupal core.

Dependancies
------------

* Poll module (Drupal core)

Installation
------------

- Install this module using the official Drupal instructions at
  http://drupal.org/documentation/install/modules-themes/modules-7

- Edit any poll and specify the correct answer under "Poll settings".

Issues
------

Bugs and Feature requests should be reported in the Issue Queue:
https://www.drupal.org/project/issues/poll_correct?categories=All

Example Usage
-------------

To indicate which answer was the correct one in poll results:

/**
 * Prepares variables for all poll result templates.
 *
 * @see poll-results.tpl.php
 * @see poll-results--block.tpl.php
 */
function THEMENAME_preprocess_poll_results(&$variables) {
  $node = node_load($variables['nid']); // Note: use entity_cache module.
  $poll_results = '';
  foreach ($node->choice as $i => $choice) {
    if (!empty($choice['chtext'])) {

      $chvotes = isset($choice['chvotes']) ? $choice['chvotes'] : NULL;
      $poll_bar = theme('poll_bar', array('title' => $choice['chtext'], 'votes' => $chvotes, 'total_votes' => '', 'vote' => isset($node->vote) && $node->vote == $i, 'block' => $variables['block']));

      // Adds a div with class "correct" around the correct answer.
      if ($i == poll_correct_get($variables['nid'])) {
        $poll_results .= '<div class="correct">' . $poll_bar . '</div>';
      }
      else {
        $poll_results .= $poll_bar;
      }
    }
  }
  $variables['results'] = $poll_results;
}


License
-------

This project is GPL v2 software. See the LICENSE.txt file in this directory for
complete text.
